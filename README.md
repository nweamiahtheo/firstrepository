package byui.edu;


import java.util.*;

public class MyClass {


    public static <object, string> void main(String[] args) {

        System.out.println("--element of list--");

        List<String> list_Strings = new ArrayList<>();
        list_Strings.add("Hi");
        list_Strings.add("My");
        list_Strings.add("Name");
        list_Strings.add("is");
        list_Strings.add("Theophilus");
        list_Strings.add("Hi");
        for (String element : list_Strings) {
            System.out.println(element);
        }

        System.out.println("--element of set--");
        Set<String> set = new TreeSet<>();
        {
            set.add("Hi");
            set.add("Theophilus");
            set.add("is");
            set.add("my");
            set.add("name");
            for (String str : set)
                System.out.println(str);
        }
        System.out.println("--element of Queue--");
        java.util.Queue<Object> queue = new PriorityQueue<>();
        queue.add("Hi");
        queue.add("Hi");
        queue.add("Theophilus");
        queue.add("is");
        queue.add("my");
        queue.add("name");

        // iterate the Priority Queue
        for (Object element : queue) {
            System.out.println(element);
        }

        System.out.println("--element of map");
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "Hi");
        map.put(2, "My");
        map.put(3, "Name");
        map.put(4, "is");
        map.put(5, "Theophilus");

        for (int i = 1; i < 6; i++) {


            string Map = (string) map.get(i);
            System.out.println(Map);

        }

        System.out.println("-- List using Generics --");
        List<Movies> myList = new LinkedList<Movies>();
        myList.add(new Movies("The Purge:  - Anachy", "James Demonaco"));
        myList.add(new Movies("Avengers infinity war", "Kelvin Feige"));
        myList.add(new Movies("Twilight", "Wyck Godfrey"));


        for (Movies dvd : myList) {
            System.out.println(dvd);

        }
    }
